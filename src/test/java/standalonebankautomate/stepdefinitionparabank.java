package standalonebankautomate;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.PageFactory;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;


public class stepdefinitionparabank {

	static WebDriver driver;
	
	@Given("browser open")
	public static void browser_open() {
		try
		{

			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			
			
			
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://driver.png");
			FileHandler.copy(scrfile, desfile);
			Thread.sleep(2000);

		} 
		catch (Exception e) 
		{
			System.out.println("invalid browser");
		}
	}

	@And("go to parabank url")
	public static  void go_to_parabank_url() {
		try {
			 driver.get("https://parabank.parasoft.com/parabank/index.htm");
		
			
		} 
		catch (Exception e) 
		{
			System.out.println("wrong url");		}
	}


	@When("enter credentials")
	public static  void enter_credentials() {

		try {
			
			PageFactory.initElements(driver, loginpageobjectmodel.class);
			loginpageobjectmodel.uname.sendKeys("santhosh");
	      	loginpageobjectmodel.pword.sendKeys("san123@K");
	      	
	      	
	      	TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://login.png");
			FileHandler.copy(scrfile, desfile);
			Thread.sleep(2000);
	      	
		} 
		catch (Exception e)
		{
			System.out.println("check your credentials");
		}
	}
	
	@Then("click login")
	public static void click_login() {

		try {
			
			loginpageobjectmodel.submit.click();
			
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://mainpage.png");
			FileHandler.copy(scrfile, desfile);
			Thread.sleep(2000);
			

		}
		catch (Exception e)
		{
			System.out.println("invalid usercredentials");
		}

	}

//@Createaccount  


	@Given("click open account linktext")
	public static void click_open_account_linktext() {

		try {
			
		createaccountpom.openaccount.click();
		
		utils.driversleep();
		TakesScreenshot screenshot=(TakesScreenshot) driver;
		File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
		
		File desfile=new File("D://createaccount.png");
		FileHandler.copy(scrfile, desfile);

		
		
		
		
			
		} 
		catch (Exception e) 
		{
			System.out.println("can't open new account ");
		}

	}

	@And("open account")
	public static void open_account() {

		try {
			
			
			createaccountpom.openaccountsucess.click();
			utils.driversleep();
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://accountcreated.png");
			FileHandler.copy(scrfile, desfile);

			
			
		}  
		catch (Exception e)
		{
			System.out.println("can't click");	
		}


	}
	
	//@Accountoverview

	@Given(":click Accountoverview linktext to view accounts")
	public static void click_accountoverview_linktext_to_view_accounts() {

		try {
			
			accountoverviewpom.accountoverview.click();
			utils.driversleep();
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://accountoverview.png");
			FileHandler.copy(scrfile, desfile);

			
			
			
		} catch (Exception e) {
			System.out.println("can't view account");
		}
	}

//transferfunds
	@Given("click on transfer fund link text")
	public static void click_on_transfer_fund_link_text() {
		try {
			driver.findElement(By.linkText("Transfer Funds")).click();
			utils.driversleep();

		} 
		catch (Exception e)
		{
			System.out.println("tranfer fund can't clickable");
		}
	}

	@Given("enter amount")
	public static void enter_amount() {
		try {
			driver.findElement(By.id("amount")).sendKeys("2");
			utils.driversleep();
		}
		catch (Exception e) 
		{
			System.out.println("amount can't enter");   
		}

	}

	@Then("click submit button")
	public static void click_submit_button() {
		try {
			driver.findElement(By.xpath("//input[@class='button']")).click();
			utils.driversleep();
		} 
		catch (Exception e)
		{
			System.out.println("can't submit");
		}
	}



	@Given("click on the bill payment link text")
	public static void click_on_the_bill_payment_link_text() {

		try {
			driver.findElement(By.linkText("Bill Pay")).click();
			utils.driversleep();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();

		}
	}

	@And("enter payee name")
	public static void enter_payee_name() {

		try {
			driver.findElement(By.name("payee.name")).sendKeys("santhosh");
			utils.driversleep();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();

		}
	}

	@And("enter address")
	public static void enter_address() {

		try {
			driver.findElement(By.name("payee.address.street")).sendKeys("venu street patel nager");
			utils.driversleep();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@And("enter city")
	public static void enter_city() {

		try {
			driver.findElement(By.name("payee.address.city")).sendKeys("chennai");
			utils.driversleep();
		} 
		catch (Exception e) 
		{

			e.printStackTrace();
		}
	}

	@And("enter state")
	public static void enter_state() {
		try {
			driver.findElement(By.name("payee.address.state")).sendKeys("tamilnadu");
			utils.driversleep();
		} 
		catch (Exception e) 
		{

			e.printStackTrace();
		}
	}

	@And("enter zipcode")
	public static void enter_zipcode() {

		try {
			driver.findElement(By.name("payee.address.zipCode")).sendKeys("600045");
			utils.driversleep();
		} 
		catch (Exception e)
		{

			e.printStackTrace();
		}
	}

	@And("enter phonenumber")
	public static void enter_phonenumber() {

		try {
			driver.findElement(By.name("payee.phoneNumber")).sendKeys("9891692310");
			utils.driversleep();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();

		}
	}

	@And("enter account number")
	public static void enter_account_number() {

		try {
			driver.findElement(By.name("payee.accountNumber")).sendKeys("17229");
			driver.findElement(By.name("verifyAccount")).sendKeys("17229");
			utils.driversleep();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();

		}
	}


	@And("enter account")
	public static void enter_account() {

		try {
			driver.findElement(By.name("amount")).sendKeys("123");
			utils.driversleep();
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();

		}
	}

	@Then("click to send payment")
	public static void click_to_send_payment() {
		try {
			driver.findElement(By.xpath("//input[@class='button']")).click();
			utils.driversleep();
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://transferfund.png");
			FileHandler.copy(scrfile, desfile);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

//find transaction
	@Given("click on find transaction link text")
	public static void click_on_find_transaction_link_text() {
		try {
			driver.findElement(By.linkText("Find Transactions")).click();
			utils.driversleep();
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Given("find transaction by id")
	public static void find_transaction_by_id() {
		try {
			driver.findElement(By.xpath("(//input[contains(@class,'input ng-pristine')])[1]")).sendKeys("18339");
			utils.driversleep();
			driver.findElement(By.xpath("(//button[@type='submit'])[1]")).click();
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://transactionbyid.png");
			FileHandler.copy(scrfile, desfile);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Given("find transaction by date")
	public static void find_transaction_by_date() {

		try {
			driver.findElement(By.linkText("Find Transactions")).click();		
			utils.driversleep();
			driver.findElement(By.xpath("(//input[contains(@class,'input ng-pristine')])[2]")).sendKeys("10-02-2022");
			utils.driversleep();
			driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
			utils.driversleep();
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://transactionbydate.png");
			FileHandler.copy(scrfile, desfile);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	@Given("find transaction by date range")
	public static void find_transaction_by_date_range() {

		try {
			driver.findElement(By.linkText("Find Transactions")).click();
			utils.driversleep();
			driver.findElement(By.xpath("(//input[contains(@class,'input ng-pristine')])[3]")).sendKeys("10-02-2022");
			utils.driversleep();
			driver.findElement(By.xpath("//input[@ng-model='criteria.toDate']")).sendKeys("10-02-2022");
			utils.driversleep();
			driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
			utils.driversleep();
			
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://transactionbydaterange.png");
			FileHandler.copy(scrfile, desfile);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	@Given("find transaction by amount")
	public static void find_transaction_by_amount() {

		try {
			driver.findElement(By.linkText("Find Transactions")).click();
			utils.driversleep();

			driver.findElement(By.xpath("//input[@ng-model='criteria.amount']")).sendKeys("122");
			utils.driversleep();
			driver.findElement(By.className("button")).click();
			utils.driversleep();
			
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://transactionbyamount.png");
			FileHandler.copy(scrfile, desfile);
			
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}


//request loan

	@Given("click on request loan link text")
	public static void click_on_request_loan_link_text() {


		try {
			driver.findElement(By.partialLinkText("Loan")).click();
			utils.driversleep();
			
			
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@And("enetr loan amount")
	public static void enetr_loan_amount() {

		try {
			driver.findElement(By.id("amount")).sendKeys("123");
			utils.driversleep();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	@And("enter downpsyment")
	public static void enter_downpsyment() {


		try {
			driver.findElement(By.id("downPayment")).sendKeys("2");
			utils.driversleep();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Then("click on apply now")
	public static void click_on_apply_now() {

		try {
			driver.findElement(By.xpath("//input[@value='Apply Now']")).click();
			utils.driversleep();
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://requestloan.png");
			FileHandler.copy(scrfile, desfile);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	@Given("click logout linktext")
	public static void click_logout_linktext() {
		try {
			driver.findElement(By.xpath("//div/div/div/ul/li/a[text()='Log Out']")).click();
			utils.driversleep();
			TakesScreenshot screenshot=(TakesScreenshot) driver;
			File scrfile=screenshot.getScreenshotAs(OutputType.FILE);
			
			File desfile=new File("D://logout.png");
			FileHandler.copy(scrfile, desfile);
			
			
//			driver.close();
//			driver.quit();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}


}


