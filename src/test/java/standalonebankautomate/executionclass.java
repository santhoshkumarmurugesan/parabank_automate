package standalonebankautomate;

import org.junit.runner.RunWith;
import io.cucumber.junit.*;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/feature/bddframework.feature",
glue={"standalonebankautomate"},
//tags=("@Login and @Createaccount and @Accountoverview and @transferfunds and @Billpay and  @findtransaction and @requestloan and @logout")
//tags="@All",
plugin= {"pretty",
		"json:target/myreport/report.json",
		"html:target/myreport/report.html"}
		)
public class executionclass {

}
